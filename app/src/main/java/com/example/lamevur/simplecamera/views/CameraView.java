package com.example.lamevur.simplecamera.views;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.hardware.Camera;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import java.io.IOException;
import java.util.List;


public class CameraView extends SurfaceView implements SurfaceHolder.Callback {
    private final static String LOG_TAG = "SimpleCamera_CameraView";
    Camera mCamera;
    SurfaceHolder mHolder;
    int mCurrentID;
    private final Context mContext;
    private final boolean FULL_SCREEN = false;

    public CameraView(Context context) {
        super(context);
        mContext = context;
        mCurrentID = Camera.CameraInfo.CAMERA_FACING_FRONT;

        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public CameraView(Context context, int camId) {
        super(context);
        mContext = context;
        mCurrentID = camId;
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        mCamera = cameraOpen(mCurrentID);
        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException ioe) {

        } catch (NullPointerException npe) {
            return;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Trying to stop not started preview");
        }

        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
            return;
        }

        setCamToDisplayOrientation();
        setPreviewToViewSize(FULL_SCREEN);

        requestLayout();
        mCamera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if(mCamera == null) return;
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    protected Camera cameraOpen(int camID) {
        Camera cam = null;

        try {
            cam = Camera.open(camID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cam;
    }

    protected void setCamToDisplayOrientation() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(mCurrentID, info);

        int rotation = ((WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay().getRotation();

        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result = 0;

        if(mCurrentID == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;
        } else {
            result = (info.orientation - degrees + 360) % 360;
        }

        mCamera.setDisplayOrientation(result);
    }

    protected void setPreviewToViewSize(final boolean FULL_SCREEN) {
        View parent = (View)getParent();
        setPreviewSize(parent.getWidth(), parent.getHeight());

        RectF parentRect = new RectF();
        parentRect.set(0, 0, parent.getWidth(), parent.getHeight());

        RectF previewRect = new RectF();
        Camera.Size size = mCamera.getParameters().getPreviewSize();
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        if((wm.getDefaultDisplay().getRotation() == Surface.ROTATION_0)
                || (wm.getDefaultDisplay().getRotation() == Surface.ROTATION_180)) {
            previewRect.set(0, 0, size.height, size.width);
        } else {
            previewRect.set(0, 0, size.width, size.height);
        }

        Matrix matrix = new Matrix();
        if(FULL_SCREEN) {
            matrix.setRectToRect(parentRect, previewRect, Matrix.ScaleToFit.START);
            matrix.invert(matrix);
        } else {
            matrix.setRectToRect(previewRect, parentRect, Matrix.ScaleToFit.START);
        }

        matrix.mapRect(previewRect);

        getLayoutParams().height = (int)previewRect.bottom;
        getLayoutParams().width = (int)previewRect.right;
    }

    protected void setPreviewSize(int wantedWidth, int wantedHeight) {
        List<Camera.Size> supportedSizes = mCamera.getParameters().getSupportedPreviewSizes();
        Log.d(LOG_TAG, "Configuring preview size to " + wantedWidth + "x" + wantedHeight);

        Camera.Size curSize = supportedSizes.get(0);
        float reqDelta = (Math.abs(wantedHeight - curSize.height)
                + Math.abs(wantedWidth + curSize.width))
                /2;
        Log.d(LOG_TAG, "For size " + curSize.width + "x" + curSize.height + " delta is " + reqDelta);
        int reqSize = 0;

        for(int i = 1; i < supportedSizes.size(); ++i) {
            curSize = supportedSizes.get(i);
            float curDelta = (Math.abs(curSize.width - wantedWidth)
                    + Math.abs(curSize.height - wantedHeight))
                    / 2 ;
            Log.d(LOG_TAG, "For size " + curSize.width + "x" + curSize.height + " delta is " + curDelta);
            if(reqDelta > curDelta) {
                reqDelta = curDelta;
                reqSize = i;
            }
        }

        Camera.Parameters parameters = mCamera.getParameters();
        Camera.Size mPreviewSize = mCamera.getParameters().getSupportedPreviewSizes().get(reqSize);
        parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
        Log.d(LOG_TAG, "Setup new camera preview resolution: " + mPreviewSize.width + "x" + mPreviewSize.height);
        mCamera.setParameters(parameters);
    }

    public int getCameraID() {
        return mCurrentID;
    }
}

