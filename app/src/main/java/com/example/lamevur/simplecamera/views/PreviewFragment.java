package com.example.lamevur.simplecamera.views;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.lamevur.simplecamera.R;

/**
 * Created by lamevur on 18.01.15.
 */
public class PreviewFragment extends Fragment implements View.OnClickListener {

    private final String LOG_TAG = "SIMPLECAMERA_PREVIEWFRAGMENT";
    private final String SAVE_CAMERA = "CameraID";

    private ImageButton mSwitchButtons;
    private CameraView mPreview;
    private Context mContext;
    private RelativeLayout mRootLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        Log.d(LOG_TAG, "onCreate() start");

        mContext = getActivity();
        View view = inflater.inflate(R.layout.fragment_preview, container, false);
        mRootLayout = (RelativeLayout) view.findViewById(R.id.fragmentpreview_layout);

        int cameraId;
        if(savedInstanceState != null) {
            cameraId = savedInstanceState.getInt("CameraID");
        } else {
            cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        }


        mPreview = new CameraView(mContext, cameraId);
        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        rlp.addRule(RelativeLayout.CENTER_IN_PARENT, mPreview.getId());
        mPreview.setLayoutParams(rlp);
//        mRootLayout.setGravity(Gravity.CENTER);
        mRootLayout.addView(mPreview);
        mRootLayout.setBackgroundColor(Color.BLACK);

        mSwitchButtons = (ImageButton)view.findViewById(R.id.fragmentpreview_imbtn_switchcamera);
        mSwitchButtons.setOnClickListener(this);
        mSwitchButtons.bringToFront();

        Log.d(LOG_TAG, "onCreate() finish");
        return view;
    }

    @Override
    public void onResume() {
        Log.d(LOG_TAG, "onResume() start");
        super.onResume();

        Log.d(LOG_TAG, "onResume() finish");
    }

    @Override
    public void onPause() {
        Log.d(LOG_TAG, "onPause() start");
        super.onPause();

        Log.d(LOG_TAG, "onPause() finish");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_CAMERA, mPreview.getCameraID());
    }

    @Override
    public void onClick(View v) {
        try {
            this.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        int curr_id = mPreview.getCameraID();
        mPreview.surfaceDestroyed(mPreview.getHolder());
        mRootLayout.removeView(mPreview);

        if(curr_id == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            mPreview = new CameraView(mContext, Camera.CameraInfo.CAMERA_FACING_BACK);
        }
        else {
            mPreview = new CameraView(mContext, Camera.CameraInfo.CAMERA_FACING_FRONT);
        }

        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        rlp.addRule(RelativeLayout.CENTER_IN_PARENT, mPreview.getId());
        mPreview.setLayoutParams(rlp);
        mRootLayout.addView(mPreview);
        mSwitchButtons.bringToFront();
    }
}
